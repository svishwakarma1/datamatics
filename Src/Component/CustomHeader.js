import React,{ useState } from 'react';
import { View, Text, TouchableOpacity, Image, Modal, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';

import TextView from './TextView';
import ProfileMenu from './ProfileMenu';
import { clearState } from './../Redux/Actions/loginAction';
import { resetStack } from './../Services/CommonFunctions';

const CustomHeader = (props) => {
	let { title, home } = props;
	const [ option, setOption ] = useState(false);

	const onPressBack = () => {
		props.navigation.goBack();
	}

	const onPressLogout = () => {
		props.clearState();
		resetStack(props.navigation,'Login');
	}

	return (
		<View style={{height:60,backgroundColor:'white',flexDirection:'row',alignItems:'center',paddingHorizontal:15,justifyContent:'space-between'}}>
			<TouchableOpacity
				onPress={() => {
					if(!home)
						onPressBack();
				}}
				activeOpacity={1}
				hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
			>
				{ home && <Image source={require('./../assets/Images/home.png')} style={{height:20,width:20,resizeMode:'contain'}}/> }
				{ !home && <Image source={require('./../assets/Images/back.png')} style={{height:20,width:20,resizeMode:'contain'}}/> }
			</TouchableOpacity>
			<TextView style={{fontSize:18}}>{title}</TextView>
			<TouchableOpacity
				onPress={() => setOption(true)}
				activeOpacity={1}
				hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
			>
				<Image source={require('./../assets/Images/more.png')} style={{height:20,width:20,resizeMode:'contain'}}/>
			</TouchableOpacity>

			<Modal
				animationType="fade"
		        transparent={true}
		        visible={option}
		        onRequestClose={() => setOption(false)}
			>
				<SafeAreaView style={{flex:1}}>
					<TouchableOpacity style={{flex:1,backgroundColor:'rgba(0, 0, 0, 0.5)'}} onPress={() => setOption(false)} activeOpacity={1}/>
					<View style={{height:80,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
						<ProfileMenu title="Profile" onPress={() => props.navigation.navigate('Profile')}/>
						<ProfileMenu title="Logout" onPress={() => onPressLogout()}/>
					</View>
				</SafeAreaView>
			</Modal>

		</View>
	)
}

const mapDispatchToProps = (dispatch) => {
	return {
		clearState: () => dispatch(clearState())
	}
}

export default connect(null,mapDispatchToProps)(CustomHeader);

