import React from 'react';
import { View, Text } from 'react-native';

const TextView = (props) => {
	return (
		<Text style={[{fontSize:14},props.style]}>{props.children}</Text>
	)
}

export default TextView;