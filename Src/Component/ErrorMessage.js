import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import TextView from './TextView';

const ErrorMesasge = (props) => {
	
	if(props.error!=null){
		return (
			<View style={{marginTop:10}}>
				<TextView style={{color:'red'}}>{props.error}</TextView>
			</View>
		)
	}else{
		return null;
	}

}


const mapStateToProps = (state) => {
	return {
		error:state.login.errorMessage
	}
}

export default connect(mapStateToProps,null)(ErrorMesasge);