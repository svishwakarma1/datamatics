import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import TextView from './TextView';

const ProfileMenu = (props) => {

	let { title, onPress } = props;
	return (
		<TouchableOpacity style={{paddingVertical:10,alignItems:'center',width:'100%'}}
			onPress={() => {
				if(onPress)
					onPress();
			}}
			activeOpacity={1}
		>
			<TextView style={{fontSize:16}}>{title}</TextView>
		</TouchableOpacity>
	)
}


export default ProfileMenu;