import React from 'react';
import { TextInput, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const EditText = (props) => {
	let { value, onChangeText, style, placeholder } = props;
	return (
		<TextInput 
			style={[{height:40,borderColor:'grey',borderWidth:1,width:width * 0.8,borderRadius:20,paddingHorizontal:15,color:'black'},style]}
	        onChangeText={(text) => {
	        	if(onChangeText)
	        		onChangeText(text)
	        }}
	        value={value}
	        placeholder={placeholder}
	        placeholderTextColor={"black"}
		/>
	)
}

export default EditText;