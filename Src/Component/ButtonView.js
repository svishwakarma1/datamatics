import React from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';

import TextView from './TextView';

const { width, height } = Dimensions.get('window');

const ButtonView = (props) => {
	let { children, style, textStyle, onPress } = props;
	return (
		<TouchableOpacity
			onPress={() => {
				if(onPress)
					onPress();
			}}
			activeOpacity={0.9}
			style={[{height:40,width:width * 0.8,alignItems:'center',justifyContent:'center',borderWidth:1,borderColor:'grey',borderRadius:20},style]}
		>
			<TextView style={textStyle}>{children}</TextView>
		</TouchableOpacity>
	)
}

export default ButtonView;