import React,{ useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Animated } from 'react-native';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';

import TextView from './../Component/TextView';
import { networkStatus } from './../Redux/Actions/networkAction';

const FrameLayout = (props) => {

	const [ fadeAnim ] = useState(new Animated.Value(0));
	const [ connected, setConnected ] = useState(false);
	let { children } = props;

	useEffect(() => {
	    const unsubscribe = NetInfo.addEventListener((state) => {
            setConnected(state.isInternetReachable);
            props.networkStatus(state.isInternetReachable);
            fadeIn();
        });

	    return () => {
            unsubscribe();
        }
  	}, [connected])

	const fadeIn = () => {
		Animated.timing(fadeAnim, {
	      	toValue: 1,
	      	duration: 1000,
	      	useNativeDriver:true
	    }).start(() => fadeOut());
	}

	const fadeOut = () => {
		Animated.timing(fadeAnim, {
	      	toValue: 0,
	      	duration: 5000,
	      	useNativeDriver:true
	    }).start();
	}

	return (
		<SafeAreaView style={{flex:1}}>
			{ children }

			{ 
				<Animated.View style={{position:'absolute',bottom:0,height:30,alignItems:'center',justifyContent:'center',backgroundColor:(connected)?'green':'red',width:'100%',opacity: fadeAnim}}>
					<TextView style={{color:'white'}}> Device is { (connected)?'online':'offline' } </TextView>
				</Animated.View>
			}
		</SafeAreaView>
	)
}

const mapDispatchToProps = (dispatch) => {
	return {
		networkStatus: data => dispatch(networkStatus(data))
	}
}

export default connect(null,mapDispatchToProps)(FrameLayout);



