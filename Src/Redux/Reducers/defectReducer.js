import { UPDATE_LIST } from './../Constant';

const initialState = {
    list:[]
}


const defectReducer = (state = initialState, action) => {
	switch(action.type){
        case UPDATE_LIST:
            return{
                ...state,
                list: action.payload
            }
        default:
            return state;
    }
    return state;
}

export default defectReducer;