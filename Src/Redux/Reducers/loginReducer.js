import { LOGIN } from './../Constant';

const initialState = {
	isUserLoggedIn:false,
	token:null,
    authenticating:false,
    userDetail:null,
    errorMessage:null
}


const loginReducer = (state = initialState, action) => {
	switch(action.type){
        case LOGIN.UPDATE_AUTHORIZEDDATA:
            return{
                ...state,
                token: action.payload
            }
        case LOGIN.UPDATE_ISUSERLOGGEDIN:
            return {
                ...state,
                isUserLoggedIn: action.payload,
            }
        case LOGIN.AUTHENTICATING:
            return{
                ...state,
                authenticating: action.payload,
            }
        case LOGIN.UPDATE_USER_DETAIL:{
            return{
                ...state,
                userDetail:action.payload
            }
        }
        case LOGIN.UPDATE_ERROR_MESSAGE:{
            return{
                ...state,
                errorMessage:action.payload
            }
        }
        default:
            return state;
    }
    return state;
}

export default loginReducer;