import { NETWORK } from './../Constant';

const initialState = {
	networkConnect:true
}


const networkReducer = (state = initialState, action) => {
	switch(action.type){
        case NETWORK:
            return{
                ...state,
                networkConnect: action.payload
            }

        default:
            return state;
    }
    return state;
}

export default networkReducer;