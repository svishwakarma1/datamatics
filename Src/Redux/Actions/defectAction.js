import { UPDATE_LIST } from './../Constant';

export const updateList = (data) => {
	return {
		type:UPDATE_LIST,
		payload:data
	}
}