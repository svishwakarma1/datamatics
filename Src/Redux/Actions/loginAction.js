import { LOGIN } from './../Constant';
import API from './../../Services/Api';

export const authenticate = (data) => {
	return (dispatch) => {
		dispatch(isLoading(true));
	    API.post('Token',{ Username:data.username, Password:data.password})
	    .then((res) => {
	    	let { token, phoneNumber, userName, firstName, lastName, email } = res.data;
	    	let detail = {number:phoneNumber,username:userName,firstname:firstName,lastname:lastName,email:email}
	    	dispatch(updateUserDetail(detail))
	    	dispatch(updateAuth(token))
	    	dispatch(updateLoginState(true))
	    	dispatch(isLoading(false));
	    	dispatch(updateErrorMessage(null))
	    }).catch((error) => {
	    	dispatch(updateErrorMessage(error.response.data))
	    	dispatch(isLoading(false));
	    })
  	}
}

export const updateUserDetail = (data) => {
	return {
		type:LOGIN.UPDATE_USER_DETAIL,
		payload:data
	}
}

export const updateErrorMessage = (data) => {
	return {
		type:LOGIN.UPDATE_ERROR_MESSAGE,
		payload:data
	}
}

export const updateLoginState = (data) => {
	return {
		type:LOGIN.UPDATE_ISUSERLOGGEDIN,
		payload:data
	}
}

export const updateAuth = (data) => {
	return {
		type:LOGIN.UPDATE_AUTHORIZEDDATA,
		payload:data
	}
}

export const isLoading = (data) => {
	return {
		type:LOGIN.AUTHENTICATING,
		payload:data
	}
}

export const clearState = () => {
	return (dispatch) => {
		dispatch(updateErrorMessage(null))
		dispatch(updateAuth(null))
		dispatch(updateLoginState(false))
		dispatch(isLoading(false))
		dispatch(updateUserDetail(null))
	}
}