import { NETWORK } from './../Constant';

export const networkStatus = (data) => {
	return {
		type:NETWORK,
		payload:data
	}
}