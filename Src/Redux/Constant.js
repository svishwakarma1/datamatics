export const LOGIN = {
	UPDATE_AUTHORIZEDDATA:'UPDATE_AUTHORIZEDDATA',
	UPDATE_ISUSERLOGGEDIN:'UPDATE_ISUSERLOGGEDIN',
	AUTHENTICATING:'AUTHENTICATING',
	UPDATE_USER_DETAIL:'UPDATE_USER_DETAIL',
	UPDATE_ERROR_MESSAGE:'UPDATE_ERROR_MESSAGE'
}

export const NETWORK = 'NETWORK';

export const UPDATE_LIST = 'UPDATE_LIST';