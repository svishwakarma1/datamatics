import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage';

import loginReducer from './Reducers/loginReducer';
import networkReducer from './Reducers/networkReducer';
import defectReducer from './Reducers/defectReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const rootReducer = combineReducers({
    login: loginReducer,
    network:networkReducer,
    defect:defectReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer)
  
const middleware = applyMiddleware(thunk);
const store = createStore(persistedReducer, middleware);
const persistor = persistStore(store);

export { persistor, store };


