import React,{ Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { clearState } from './Redux/Actions/loginAction';
import LoginScreen from './Screens/LoginScreen';
import HomeScreen from './Screens/HomeScreen';
import ProfileScreen from './Screens/ProfileScreen';
import CustomHeader from './Component/CustomHeader';

const Stack = createNativeStackNavigator();

class Router extends Component {

	constructor(props){
		super(props);
	}

	componentDidMount(){
		//this.props.clearState();
	}

	render(){
		let { isLogged } = this.props;
		return (
			<NavigationContainer>
	          	<Stack.Navigator initialRouteName={(isLogged)?"Home":"Login"}>
		            <Stack.Screen name="Login" component={LoginScreen} options={{header: () => (<View/>)}}/>
		            <Stack.Screen name="Home" component={HomeScreen} options={{header: (navigation) => (<CustomHeader title="Home" home {...navigation}/>)}} />
		            <Stack.Screen name="Profile" component={ProfileScreen} options={{header: (navigation) => (<CustomHeader title="Profile" {...navigation}/>)}} />
	          	</Stack.Navigator>
	        </NavigationContainer>
		)
	}
}


const mapStateToProps = (state) => {
	return {
		isLogged:state.login.isUserLoggedIn
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		clearState: () => dispatch(clearState())
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Router);



