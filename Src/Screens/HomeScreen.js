import React,{ useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
	
import Api from './../Services/Api';
import TextView from './../Component/TextView';
import FrameLayout from './../Layout/FrameLayout';
import { updateList } from './../Redux/Actions/defectAction';

const HomeScreen = (props) => {

	const [ refresh, setRefresh ] = useState(false);
	const [ loader, setLoader ] = useState(false);
	const [ selected, setSelected ] = useState(null);
	const [ page, setPage ] = useState(1);

	useEffect(() => {
		loadList();
	},[])

	useEffect(() => {
		loadList();
	},[page,props.networkStatus])

	const onRefresh = () => {
		setRefresh(true);
		setPage(1)
	}

	const loadList = () => {
		let { token, networkStatus } = props;
        if(networkStatus){
			setLoader(true);
        	Api.post('Dashboard/GetAllDefects',
			{ pageIndex:page, pageSize:10 },
			{
	            headers: {
	                Authorization: 'Bearer ' + token,
	                Accept: 'application/json'
	            }
	    	}).then((res) => {
	    		let { count, data } = res.data;
	    		props.updateList(page === 1 ? data : [...props.defectList, ...data])
	    		setLoader(false);
	    		setRefresh(false);
			}).catch((error) => {
				setLoader(false);
				setRefresh(false);
			})
        }
	}

	const renderItem = ({item,index}) => {
		let { failureId, createdBy, defectText, assignedTo, updatedOn, status } = item;
		return (
			<TouchableOpacity style={styles.mainCardView}
				onPress={() => {
					setSelected(index)	
				}}
				activeOpacity={1}
			>
				<TextView>{failureId || '-' }</TextView>
				<TextView style={{marginTop:5}}>{createdBy || '-'}</TextView>
				<TextView style={{marginTop:5}}>{defectText || '-'}</TextView>

				{
					selected == index &&
					<View style={{borderTopWidth:1,borderTopColor:'grey',paddingTop:10,marginTop:10}}>
						<TextView>{assignedTo?.firstName || '-'} {assignedTo?.lastName || '-'}</TextView>
						<TextView>{updatedOn || '-'}</TextView>
						<TextView>{status || '-'}</TextView>
					</View>
				}
			</TouchableOpacity>
		)
	}

	const renderFooter = () => {
    	if(loader){
    		return <ActivityIndicator color="black" size="large"/>
    	}else{
    		return null
    	}
  	}

	return (
		<FrameLayout>
			<View style={styles.container}>
				<FlatList
		        	data={props.defectList}
		        	renderItem={renderItem}
		        	keyExtractor={item => item.id}
		        	onEndReached={() => setPage(page + 1)}
	            	onEndReachedThreshold ={0.5}
	            	onRefresh={() => onRefresh()}
	    			refreshing={refresh}
		        	ItemSeparatorComponent={() => <View style={{marginTop:10}}/>}
		        	ListFooterComponent={renderFooter}
		      	/>
			</View>
		</FrameLayout>
	)
}

const mapStateToProps = (state) => {
	return {
		token:state.login.token,
		networkStatus:state.network.networkConnect,
		defectList:state.defect.list
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		updateList:data => dispatch(updateList(data))
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  mainCardView: {
    alignSelf: "center",
    backgroundColor: "white",
    borderRadius: 4,
    width: '90%',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5 ,
    padding:10
  }
});





