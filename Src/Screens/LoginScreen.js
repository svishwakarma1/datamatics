import React,{ useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import EditText from './../Component/EditText';
import ButtonView from './../Component/ButtonView';
import TextView from './../Component/TextView';
import ErrorMessage from './../Component/ErrorMessage';
import { authenticate } from './../Redux/Actions/loginAction';
import { resetStack } from './../Services/CommonFunctions';

const LoginScreen = (props) => {

	const [ user, setUser ] = useState('testing');
	const [ password, setPassword ] = useState('Testing123');

	useEffect(() => {
		if(props.token!=null){
			resetStack(props.navigation,'Home');
		}
	},[props.token])

	const onPressLogin = () => {
		let param = { username: user, password: password };
		props.authenticate(param);
	}

	return (
		<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'white'}}>
			<View>
				<EditText 
					placeholder={"User name"}
					value={user}
					onChangeText={(text) => setUser(text)}
				/>

				<EditText 
					placeholder={"Password"}
					value={password}
					onChangeText={(text) => setPassword(text)}
					style={{marginTop:10}}
				/>

				<ButtonView
					style={{marginTop:50}}
					onPress={() => {
						if(!props.loader)
							onPressLogin();
					}}
				>
					Login
				</ButtonView>
			</View>


			<ErrorMessage />

			<Modal
				animationType="fade"
		        transparent={true}
		        visible={props.loader}
			>
				<SafeAreaView style={{flex:1}}>
					<View style={{flex:1,backgroundColor:'rgba(0, 0, 0, 0.5)'}}/>
					<View style={{height:80,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
						<View style={{flexDirection:'row',alignItems:'center'}}>
							<ActivityIndicator size="small" color="black" />
							<TextView style={{marginLeft:10}}>Authentication...</TextView>
						</View>
					</View>
				</SafeAreaView>
			</Modal>
		</View>
	)
}

const mapStateToProps = (state) => {
	return {
		loader:state.login.authenticating,
		token:state.login.token
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		authenticate: data => dispatch(authenticate(data)),
	}
}


export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen);



