import React,{ useState } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import FrameLayout from './../Layout/FrameLayout';
import TextView from './../Component/TextView';

const ProfileScreen = (props) => {
	return (
		<FrameLayout>
			<View style={{paddingTop:20,alignItems:'center'}}>
				<TextView>{props.detail?.username || '-'}</TextView>
				<TextView style={{marginTop:10}}>{props.detail?.firstname  || '-'} {props.detail?.lastname  || '-'}</TextView>
				<TextView style={{marginTop:10}}>{props.detail?.number || '-'}</TextView>
				<TextView style={{marginTop:10}}>{props.detail?.email || '-'}</TextView>
			</View>
		</FrameLayout>
	)
}


const mapStateToProps = (state) => {
	return {
		detail:state.login.userDetail
	}
}

export default connect(mapStateToProps,null)(ProfileScreen);