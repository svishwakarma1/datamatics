import * as React from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './Src/Redux/Store';
import Router from './Src/Router';

const App = (props)  => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router />  
      </PersistGate>
    </Provider>
  );
}


export default App;